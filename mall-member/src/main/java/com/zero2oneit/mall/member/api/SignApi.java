package com.zero2oneit.mall.member.api;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zero2oneit.mall.common.bean.member.MemberSign;
import com.zero2oneit.mall.common.bean.member.MemberSignRecord;
import com.zero2oneit.mall.common.bean.member.MemberSignRule;
import com.zero2oneit.mall.common.query.member.MemberSignRecordQueryObject;
import com.zero2oneit.mall.common.utils.R;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;
import com.zero2oneit.mall.member.service.MemberSignRecordService;
import com.zero2oneit.mall.member.service.MemberSignRuleService;
import com.zero2oneit.mall.member.service.MemberSignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * Description:
 *
 * @author Cmx
 * @date 2021/5/13 12:10
 */
@RestController
@RequestMapping("/api/auth/member/sign")
public class SignApi {

    @Autowired
    private MemberSignService memberSignService;

    @Autowired
    private MemberSignRecordService signRecordService;

    /**
     * 加载会员规则和个人签到信息
     * @param memberId 参数
     * @return
     */
    @PostMapping("/load")
    public R load(@RequestBody Long memberId){
        return memberSignService.load(memberId);
    }

    /**
     * 查询签到明细
     * @param qo 参数
     * @return
     */
    @PostMapping("/list")
    public R list(@RequestBody MemberSignRecordQueryObject qo){
        BoostrapDataGrid page = signRecordService.recordList(qo);
        return R.ok("加载成功", page);
    }

    /**
     * 签到
     * @param qo 参数
     * @return
     */
    @PostMapping("/sign")
    public R sign(@RequestBody MemberSign qo){
        return memberSignService.sign(qo);
    }

}
