package com.zero2oneit.mall.member.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zero2oneit.mall.common.bean.member.PrizeWinMapping;
import com.zero2oneit.mall.common.query.member.PrizeWinMappingQueryObject;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;
import com.zero2oneit.mall.member.mapper.PrizeWinMappingMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.zero2oneit.mall.member.service.PrizeWinMappingService;

import java.util.Collections;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-05-25
 */
@Service
public class PrizeWinMappingServiceImpl extends ServiceImpl<PrizeWinMappingMapper, PrizeWinMapping> implements PrizeWinMappingService {

    @Autowired
    private PrizeWinMappingMapper prizeWinMappingMapper;

    @Override
    public BoostrapDataGrid pageList(PrizeWinMappingQueryObject qo) {
        //查询总记录数
        int total = prizeWinMappingMapper.selectTotal(qo);
        return new BoostrapDataGrid(total, total == 0 ? Collections.EMPTY_LIST : prizeWinMappingMapper.selectRows(qo));
    }
}