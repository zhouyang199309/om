package com.zero2oneit.mall.member.controller;

import com.zero2oneit.mall.common.query.member.InfoQueryObject;
import com.zero2oneit.mall.common.query.member.MemberCouponQueryObject;
import com.zero2oneit.mall.common.utils.R;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;
import com.zero2oneit.mall.member.service.MemberCouponService;
import com.zero2oneit.mall.member.service.MemberInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-04-21
 */
@RestController
@RequestMapping("/admin/member")
public class MemberController {

    @Autowired
    private MemberInfoService memberService;

    @Autowired
    private MemberCouponService couponService;


    /**
     * 查询会员信息列表
     * @param qo
     * @return
     */
    @PostMapping("/list")
    public BoostrapDataGrid list(@RequestBody InfoQueryObject qo) {
        return memberService.pageList(qo);
    }

    /**
     * 重置会员登录密码
     * @param memberId
     * @return
     */
    @PostMapping("/resetPwd")
    public R resetPwd(@RequestParam("memberId") String memberId, @RequestParam("type") String type){
        return memberService.resetPwd(memberId, type);
    }

    /**
     * 禁用/撤销禁用
     * @param memberId
     * @param type
     * @return
     */
    @PostMapping("/status")
    public R status(@RequestParam("memberId") String memberId, @RequestParam("type") String type){
        return memberService.status(memberId, type);
    }

    /**
     * 赠送优惠券
     * @param memberIds
     * @param couponId
     * @return
     */
    @PostMapping("/coupon")
    public R coupon(@RequestParam("memberIds") String memberIds, @RequestParam("couponId") String couponId){
        return memberService.coupon(memberIds, couponId);
    }

    /**
     * 加载会员优惠券列表
     * @param qo 参数
     * @return
     */
    @PostMapping("/coupon/list")
    public BoostrapDataGrid list(@RequestBody MemberCouponQueryObject qo){
        return couponService.pageList(qo);
    }

}
